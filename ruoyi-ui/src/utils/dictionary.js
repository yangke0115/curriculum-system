const granularityOptions = [
  {
    value: 1,
    label: '教材'
  },
  {
    value: 2,
    label: '章'
  },
  {
    value: 3,
    label: '节'
  },
  {
    value: 4,
    label: '小节'
  }
]

export {
  granularityOptions
}
