import request from '@/utils/request'

// 查询SC，经典数据集
export function generateSc(query) {
  return request({
    url: '/curriculumSystemManagement/textbook/generateSC',
    method: 'post',
    data: query
  })
}

// 查询SM，最大专业数据集
export function generateSM(query) {
  return request({
    url: '/curriculumSystemManagement/textbook/generateSM',
    method: 'post',
    data: query
  })
}
