import request from '@/utils/request'

// 查询教材管理列表
export function listBooks(query) {
  return request({
    url: '/curriculumSystemManagement/textbook/list',
    method: 'get',
    params: query
  })
}

// 查询教材详情
export function bookDetail(query) {
  return request({
    url: `/curriculumSystemManagement/textbook/details?isbn=${query}`,
    method: 'get'
  })
}

// 修改教材节点信息
export function updateBookNode(query) {
  return request({
    url: `/curriculumSystemManagement/textbook/update`,
    method: 'post',
    data:query
  })
}
// 新增教材章、节的节点
export function addBookNode(query) {
  return request({
    url: `/curriculumSystemManagement/textbook/add/node`,
    method: 'post',
    data:query
  })
}

// 删除教材或教材下的节点
export function deleteBookByNode(query) {
  return request({
    url: `/curriculumSystemManagement/textbook/remove`,
    method: 'post',
    data:query
  })
}

// 通过json新增一整本教材
export function textBookImport(query) {
  return request({
    url: `/curriculumSystemManagement/textbook/import`,
    method: 'post',
    data:query
  })
}
