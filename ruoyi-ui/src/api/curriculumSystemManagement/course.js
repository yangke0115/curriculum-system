import request from '@/utils/request'

// 查询课程管理列表
export function listCourse(query) {
  return request({
    url: '/curriculumSystemManagement/course/list',
    method: 'get',
    params: query
  })
}

// 查询课程管理详细
export function getCourse(id) {
  return request({
    url: '/curriculumSystemManagement/course/' + id,
    method: 'get'
  })
}

// 新增课程管理
export function addCourse(data) {
  return request({
    url: 'curriculumSystemManagement/course/add',
    method: 'post',
    data: data
  })
}

// 修改课程管理
export function updateCourse(data) {
  return request({
    url: 'curriculumSystemManagement/course/edit',
    method: 'post',
    data: data
  })
}

// 删除课程管理
export function delCourse(id) {
  return request({
    url: '/curriculumSystemManagement/course/' + id,
    method: 'delete'
  })
}

// 导出课程管理
export function exportCourse(query) {
  return request({
    url: '/curriculumSystemManagement/course/export',
    method: 'get',
    params: query
  })
}

// 在课程下绑定教材
export function course_bookAdd(query){
  return request({
    url:'/curriculumSystemManagement/course_book/add',
    method:'post',
    data:query
  })
}
