package com.ruoyi.curriculumSystemManagement.controller;

import java.util.List;

import com.ruoyi.curriculumSystemManagement.domain.vo.MajorAndSchoolVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.service.IHncjMajorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 专业管理Controller
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@RestController
@RequestMapping("/curriculumSystemManagement/major")
public class HncjMajorController extends BaseController
{
    @Autowired
    private IHncjMajorService hncjMajorService;

    /**
     * 查询专业管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:list')")
    @GetMapping("/list")
    public TableDataInfo list(HncjMajor hncjMajor)
    {
        startPage();
        List<MajorAndSchoolVO> list = hncjMajorService.selectMajorVOList(hncjMajor);
        return getDataTable(list);
    }

    /**
     * 导出专业管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:export')")
    @Log(title = "专业管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HncjMajor hncjMajor)
    {
        List<HncjMajor> list = hncjMajorService.selectMajorList(hncjMajor);
        ExcelUtil<HncjMajor> util = new ExcelUtil<HncjMajor>(HncjMajor.class);
        return util.exportExcel(list, "专业管理数据");
    }

    /**
     * 获取专业管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(hncjMajorService.selectHncjMajorById(id));
    }

    /**
     * 新增专业管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:add')")
    @Log(title = "专业管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HncjMajor hncjMajor)
    {
        return toAjax(hncjMajorService.insertHncjMajor(hncjMajor));
    }

    /**
     * 修改专业管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:edit')")
    @Log(title = "专业管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HncjMajor hncjMajor)
    {
        return toAjax(hncjMajorService.updateHncjMajor(hncjMajor));
    }

    /**
     * 删除专业管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:major:remove')")
    @Log(title = "专业管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(hncjMajorService.deleteHncjMajorByIds(ids));
    }
}
