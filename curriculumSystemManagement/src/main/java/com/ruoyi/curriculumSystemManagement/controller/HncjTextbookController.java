package com.ruoyi.curriculumSystemManagement.controller;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseSetVO;
import com.ruoyi.curriculumSystemManagement.mapper.HncjTextbookMapper;
import com.ruoyi.curriculumSystemManagement.service.ICourseSetService;
import com.ruoyi.curriculumSystemManagement.service.IHncjTextbookService;
import com.ruoyi.curriculumSystemManagement.utils.TextBookImport;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * (HncjTextbook)表控制层
 *
 * @author songbiao
 * @since 2021-11-06 14:53:34
 */
@RestController
@RequestMapping("/curriculumSystemManagement/textbook")
public class HncjTextbookController extends BaseController {
    @Resource(name = "courseSetServiceImpl")
    private ICourseSetService courseSetService;

    /**
     * 服务对象
     */
    @Resource
    private IHncjTextbookService hncjTextbookService;

    @Resource
    private TextBookImport textBookImport;

    @Resource
    private HncjTextbookMapper textbookMapper;

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:list')")
    @GetMapping("/list")
    public TableDataInfo list(HncjTextbook textbook){
        startPage();
        return getDataTable(hncjTextbookService.listBook(textbook));
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:import')")
    @PostMapping("/import")
    public AjaxResult toImport(@RequestBody Map<String,String> map) {
        Assert.isTrue(Strings.isNotBlank(map.get("json")), "json不能为空");
        return textBookImport.process(JSON.parseObject(map.get("json")))?success("导入成功"):error("导入失败");
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:details')")
    @GetMapping("/details")
    public AjaxResult getDetailById(String isbn){
        Assert.isTrue(Strings.isNotBlank(isbn),"isbn不能为空");
        return AjaxResult.success(hncjTextbookService.listDetail(isbn));
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:add_node')")
    @PostMapping("/add/node")
    public AjaxResult insertBookNode(@RequestBody HncjTextbook textbook) {
        return hncjTextbookService.insertBookNode(textbook)?success():error();
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:update')")
    @PostMapping("/update")
    public AjaxResult updateBookNode(@RequestBody HncjTextbook textbook) {
        Assert.isTrue(Strings.isNotBlank(textbook.getCode()),"code不能为空");
        return hncjTextbookService.updateBookNode(textbook)?success():error();
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:textbook:remove')")
    @PostMapping("/remove")
    public AjaxResult removeBook(@RequestBody HncjTextbook textbook) {
        return hncjTextbookService.removeBook(textbook)?success():error();
    }

    @PostMapping("/generateSC")
    public AjaxResult generateSC(@RequestBody CourseSetVO param) {
        if (param.getMid()==null || param.getMid().length<2)
            return AjaxResult.error("请选择2个以上专业");
        return courseSetService.generateSC(param);
    }

    @PostMapping("/generateSM")
    public AjaxResult generateSM(@RequestBody CourseSetVO param) {
        return courseSetService.generateSM(param);
    }

}