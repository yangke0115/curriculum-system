package com.ruoyi.curriculumSystemManagement.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import com.ruoyi.curriculumSystemManagement.service.IHncjCourseBookService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (HncjCourseBook)表控制层
 *
 * @author makejava
 * @since 2021-11-10 11:22:23
 */
@RestController
@RequestMapping("/curriculumSystemManagement/course/book")
public class HncjCourseBookController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private IHncjCourseBookService hncjCourseBookService;

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course_book:list')")
    @GetMapping("list")
    public TableDataInfo listByCourseId(HncjCourseBook courseBook){
        startPage();
        return getDataTable(hncjCourseBookService.listByCourseId(courseBook));
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course_book:add')")
    @PostMapping("add")
    public AjaxResult add(@RequestBody HncjCourseBook courseBook) {
        return AjaxResult.success(hncjCourseBookService.insert(courseBook));
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course_book:update')")
    @PostMapping("update")
    public AjaxResult update(@RequestBody HncjCourseBook courseBook) {
        return AjaxResult.success(hncjCourseBookService.update(courseBook));
    }

//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course_book:remove')")
    @PostMapping("remove")
    public AjaxResult removeById(int id) {
        return hncjCourseBookService.deleteById(id)?success():error();
    }



}