package com.ruoyi.curriculumSystemManagement.controller;

import java.util.List;

import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;
import com.ruoyi.curriculumSystemManagement.domain.vo.MajorAndSchoolVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import com.ruoyi.curriculumSystemManagement.service.IHncjCourseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程管理Controller
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@RestController
@RequestMapping("/curriculumSystemManagement/course")
public class HncjCourseController extends BaseController
{
    @Autowired
    private IHncjCourseService hncjCourseService;

    /**
     * 查询课程管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:list')")
    @GetMapping("/list")
    public TableDataInfo list(@RequestParam(required = false) String cname,
                              @RequestParam(required = false) Long mid,
                              @RequestParam(required = false) Long sid)
    {
        startPage();
        return getDataTable(hncjCourseService.listByMajorOrSchool(cname, mid, sid));
    }

    /**
     * 导出课程管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:export')")
    @Log(title = "课程管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HncjCourse hncjCourse)
    {
        List<HncjCourse> list = hncjCourseService.selectHncjCourseList(hncjCourse);
        ExcelUtil<HncjCourse> util = new ExcelUtil<HncjCourse>(HncjCourse.class);
        return util.exportExcel(list, "课程管理数据");
    }

    /**
     * 获取课程管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(hncjCourseService.selectHncjCourseById(id));
    }

    /**
     * 新增课程管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:add')")
    @Log(title = "课程管理", businessType = BusinessType.INSERT)
    @PostMapping("add")
    public AjaxResult add(@RequestBody HncjCourse hncjCourse)
    {
        return toAjax(hncjCourseService.insertHncjCourse(hncjCourse));
    }

    /**
     * 修改课程管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:edit')")
    @Log(title = "课程管理", businessType = BusinessType.UPDATE)
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody HncjCourse hncjCourse)
    {
        Assert.notNull(hncjCourse.getId(),"课程id不能为空");
        return toAjax(hncjCourseService.updateHncjCourse(hncjCourse));
    }

    /**
     * 删除课程管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:course:remove')")
    @Log(title = "课程管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(hncjCourseService.deleteHncjCourseByIds(ids));
    }
}
