package com.ruoyi.curriculumSystemManagement.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.service.IHncjSchoolService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学校管理Controller
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@RestController
@RequestMapping("/curriculumSystemManagement/school")
public class HncjSchoolController extends BaseController
{
    @Autowired
    private IHncjSchoolService hncjSchoolService;

    /**
     * 查询学校管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:list')")
    @GetMapping("/list")
    public TableDataInfo list(HncjSchool hncjSchool)
    {
        startPage();
        List<HncjSchool> list = hncjSchoolService.selectHncjSchoolList(hncjSchool);
        return getDataTable(list);
    }

    /**
     * 导出学校管理列表
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:export')")
    @Log(title = "学校管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HncjSchool hncjSchool)
    {
        List<HncjSchool> list = hncjSchoolService.selectHncjSchoolList(hncjSchool);
        ExcelUtil<HncjSchool> util = new ExcelUtil<HncjSchool>(HncjSchool.class);
        return util.exportExcel(list, "学校管理数据");
    }

    /**
     * 获取学校管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(hncjSchoolService.selectHncjSchoolById(id));
    }

    /**
     * 新增学校管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:add')")
    @Log(title = "学校管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HncjSchool hncjSchool)
    {
        return toAjax(hncjSchoolService.insertHncjSchool(hncjSchool));
    }

    /**
     * 修改学校管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:edit')")
    @Log(title = "学校管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HncjSchool hncjSchool)
    {
        return toAjax(hncjSchoolService.updateHncjSchool(hncjSchool));
    }

    /**
     * 删除学校管理
     */
//    @PreAuthorize("@ss.hasPermi('curriculumSystemManagement:school:remove')")
    @Log(title = "学校管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(hncjSchoolService.deleteHncjSchoolByIds(ids));
    }
}
