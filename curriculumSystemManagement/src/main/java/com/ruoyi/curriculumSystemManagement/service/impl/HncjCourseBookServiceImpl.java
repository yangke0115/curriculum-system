package com.ruoyi.curriculumSystemManagement.service.impl;

import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseBookAndBookVO;
import com.ruoyi.curriculumSystemManagement.mapper.HncjCourseBookMapper;
import com.ruoyi.curriculumSystemManagement.mapper.HncjTextbookMapper;
import com.ruoyi.curriculumSystemManagement.service.IHncjCourseBookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (HncjCourseBook)表服务实现类
 *
 * @author makejava
 * @since 2021-11-10 11:22:23
 */
@Service("hncjCourseBookService")
public class HncjCourseBookServiceImpl implements IHncjCourseBookService {
    @Resource
    private HncjCourseBookMapper hncjCourseBookDao;

    @Resource
    private HncjTextbookMapper textbookMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public HncjCourseBook queryById(Integer id) {
        return this.hncjCourseBookDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<HncjCourseBook> queryAllByLimit(int offset, int limit) {
        return this.hncjCourseBookDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param hncjCourseBook 实例对象
     * @return 实例对象
     */
    @Override
    public HncjCourseBook insert(HncjCourseBook hncjCourseBook) {
        this.hncjCourseBookDao.insert(hncjCourseBook);
        return hncjCourseBook;
    }

    /**
     * 修改数据
     *
     * @param hncjCourseBook 实例对象
     * @return 实例对象
     */
    @Override
    public HncjCourseBook update(HncjCourseBook hncjCourseBook) {
        this.hncjCourseBookDao.update(hncjCourseBook);
        return this.queryById(hncjCourseBook.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.hncjCourseBookDao.deleteById(id) > 0;
    }

    @Override
    public List<CourseBookAndBookVO> listByCourseId(HncjCourseBook courseBook) {
        List<CourseBookAndBookVO> result = new ArrayList<>();
        List<HncjCourseBook> courseBooks = hncjCourseBookDao.queryAll(courseBook);
        for (HncjCourseBook book : courseBooks) {
            CourseBookAndBookVO vo = new CourseBookAndBookVO();
            BeanUtils.copyBeanProp(vo,book);
            vo.setTextbook(textbookMapper.queryById(book.getTextId()));
            result.add(vo);
        }
        return result;
    }

    @Override
    public boolean delete(HncjCourseBook courseBook) {
        return hncjCourseBookDao.delete(courseBook)>0;
    }
}