package com.ruoyi.curriculumSystemManagement.service;

import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 *
 */
public interface IHncjTextbookService {


    List<HncjTextbook> listBook(HncjTextbook condition);

    List<HncjTextbook> listDetail(String isbn);


    boolean insertBookNode(@Validated HncjTextbook textbook);

    boolean updateBookNode(HncjTextbook textbook);

    boolean removeBook(HncjTextbook textbook);

    List<HncjTextbook> listByCodes(String codes);

    boolean insertList(List<HncjTextbook> textbooks);

    HncjTextbook getParent(HncjTextbook hncjTextbook);

}
