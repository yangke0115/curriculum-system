package com.ruoyi.curriculumSystemManagement.service.impl;

import java.util.HashMap;
import java.util.List;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;
import com.ruoyi.curriculumSystemManagement.domain.vo.MajorAndSchoolVO;
import com.ruoyi.curriculumSystemManagement.service.*;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Objects;

import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.curriculumSystemManagement.mapper.HncjCourseMapper;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * 课程管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@Service
public class HncjCourseServiceImpl implements IHncjCourseService 
{
    @Autowired
    private HncjCourseMapper hncjCourseMapper;

    @Resource
    private IHncjMajorService majorService;

    @Resource
    private IHncjSchoolService schoolService;

    @Resource
    private IHncjTextbookService textbookService;

    @Resource
    private IHncjCourseBookService courseBookService;

    /**
     * 查询课程管理
     * 
     * @param id 课程管理主键
     * @return 课程管理
     */
    @Override
    public HncjCourse selectHncjCourseById(Long id)
    {
        return hncjCourseMapper.selectHncjCourseById(id);
    }

    /**
     * 查询课程管理列表
     * 
     * @param hncjCourse 课程管理
     * @return 课程管理
     */
    @Override
    public List<HncjCourse> selectHncjCourseList(HncjCourse hncjCourse)
    {
        return hncjCourseMapper.selectHncjCourseList(hncjCourse);
    }

    @Override
    public List<CourseAndMajorAndSchoolVO> listByMajorOrSchool(String cname, Long mId, Long sId) {
        TableDataInfo pageInfo = new TableDataInfo();
        HashMap<Object, Object> queryWrapper = new HashMap<>();
        queryWrapper.put("mId", mId);
        queryWrapper.put("sId", sId);
        queryWrapper.put("cName", cname);
        List<CourseAndMajorAndSchoolVO> voList = schoolService.selectCourseList(queryWrapper);
        voList.stream().parallel().forEach(vo->{
            vo.setHncjTextbookList(textbookService.listByCodes(vo.getCodes()));
        });
        return voList;
    }

    private List<MajorAndSchoolVO> selectCourseVOList(HncjCourse hncjCourse) {
        List<MajorAndSchoolVO> voList = new ArrayList<>();
        List<HncjCourse> courses = hncjCourseMapper.selectHncjCourseList(hncjCourse);
        for (HncjCourse course : courses) {
            MajorAndSchoolVO courseVO = new MajorAndSchoolVO();
            HncjMajor major = majorService.selectHncjMajorById(course.getMid());
            HncjSchool school = schoolService.selectHncjSchoolById(major.getId());
            BeanUtils.copyBeanProp(courseVO, major);
            courseVO.setSchool(school);
            voList.add(courseVO);
        }
        return voList;
    }

    /**
     * 新增课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertHncjCourse(HncjCourse hncjCourse)
    {
        int rows = hncjCourseMapper.insertHncjCourse(hncjCourse);
        String codes = hncjCourse.getCodes();
        if (rows > 0 && Strings.isNotBlank(codes)) {
            //添加到中间表
            HncjCourseBook data = new HncjCourseBook();
            data.setCId(hncjCourse.getId().intValue());
            for (String code : codes.split(",")) {
                data.setTextId(code);
                courseBookService.insert(data);
            }
        }
        return rows;
    }

    /**
     * 修改课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateHncjCourse(HncjCourse hncjCourse)
    {
        //查询codes
        String newCodes = hncjCourse.getCodes();
        if (newCodes != null) {
            HncjCourse oldCourse = selectHncjCourseById(hncjCourse.getId());
            Assert.isTrue(oldCourse!=null && oldCourse.getId()!=null,"数据异常");
            String oldCodes = oldCourse.getCodes();
            boolean toAdd;
            HncjCourseBook courseBookDTO = new HncjCourseBook();
            courseBookDTO.setCId(hncjCourse.getId().intValue());
            //如果原数据是空,则新增
            if (Strings.isBlank(oldCodes) && !newCodes.equals("")) {
                for (String n : newCodes.split(",")) {
                    courseBookDTO.setTextId(n);
                    courseBookService.insert(courseBookDTO);
                }
            }
            else if (!Objects.equals(oldCodes,newCodes)) {
                String[] oldCodeArr = oldCodes.split(",");
                String[] newCodeArr = newCodes.split(",");
                for (int i = 0; i < newCodeArr.length; i++) {
                    toAdd = true;
                    for (int j = 0; j < oldCodeArr.length; j++) {
                        if (newCodeArr[i].equals(oldCodeArr[j])) {
                            //设置一个标志位标识当前教材已经存在了
                            oldCodeArr[i] = "#";
                            toAdd = false;
                            break;
                        }
                    }
                    //不存在教材就需要添加教材
                    if (toAdd && Strings.isNotBlank(newCodeArr[i])) {
                        courseBookDTO.setTextId(newCodeArr[i]);
                        courseBookService.insert(courseBookDTO);
                    }
                }
                //没有匹配的教程就需要删除掉
                for (String o : oldCodeArr) {
                    if (!o.equals("#")) {
                        courseBookDTO.setTextId(o);
                        courseBookService.delete(courseBookDTO);
                    }
                }
            }
            hncjCourse.setCodes(newCodes);
        }
        return hncjCourseMapper.updateHncjCourse(hncjCourse);
    }

    /**
     * 批量删除课程管理
     * 
     * @param ids 需要删除的课程管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteHncjCourseByIds(Long[] ids)
    {
//        hncjCourseMapper.deleteHncjTextbookByCIds(ids);
        return hncjCourseMapper.deleteHncjCourseByIds(ids);
    }

    /**
     * 删除课程管理信息
     * 
     * @param id 课程管理主键
     * @return 结果
     */
    @Override
    public int deleteHncjCourseById(Long id)
    {
//        hncjCourseMapper.deleteHncjTextbookByCId(id);
        return hncjCourseMapper.deleteHncjCourseById(id);
    }

}
