package com.ruoyi.curriculumSystemManagement.service.impl;

import java.util.List;

import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.domain.vo.MajorAndSchoolVO;
import com.ruoyi.curriculumSystemManagement.service.IHncjSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import com.ruoyi.curriculumSystemManagement.mapper.HncjMajorMapper;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.service.IHncjMajorService;

import javax.annotation.Resource;

/**
 * 专业管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@Service
public class HncjMajorServiceImpl implements IHncjMajorService 
{
    @Autowired
    private HncjMajorMapper hncjMajorMapper;

    @Resource
    private IHncjSchoolService hncjSchoolService;

    /**
     * 查询专业管理
     * 
     * @param id 专业管理主键
     * @return 专业管理
     */
    @Override
    public HncjMajor selectHncjMajorById(Long id)
    {
        return hncjMajorMapper.selectHncjMajorById(id);
    }

    /**
     * 查询专业管理列表
     * 
     * @param hncjMajor 专业管理
     * @return 专业管理
     */
    @Override
    public List<MajorAndSchoolVO> selectMajorVOList(HncjMajor hncjMajor)
    {
        List<MajorAndSchoolVO> majorAndSchools = new ArrayList<>();
        List<HncjMajor> hncjMajors = hncjMajorMapper.selectHncjMajorList(hncjMajor);
        for (HncjMajor major : hncjMajors) {
            MajorAndSchoolVO majorAndSchool = new MajorAndSchoolVO();
            BeanUtils.copyBeanProp(majorAndSchool,major);
            HncjSchool hncjSchool = hncjSchoolService.selectHncjSchoolById(major.getSid());
            majorAndSchool.setSchool(hncjSchool);
            majorAndSchools.add(majorAndSchool);
        }
        return majorAndSchools;
    }

    @Override
    public List<HncjMajor> selectMajorList(HncjMajor hncjMajor) {
        return hncjMajorMapper.selectHncjMajorList(hncjMajor);
    }

    /**
     * 新增专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertHncjMajor(HncjMajor hncjMajor)
    {
        int rows = hncjMajorMapper.insertHncjMajor(hncjMajor);
        insertHncjCourse(hncjMajor);
        return rows;
    }

    /**
     * 修改专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateHncjMajor(HncjMajor hncjMajor)
    {
        hncjMajorMapper.deleteHncjCourseByMId(hncjMajor.getId());
        insertHncjCourse(hncjMajor);
        return hncjMajorMapper.updateHncjMajor(hncjMajor);
    }

    /**
     * 批量删除专业管理
     * 
     * @param ids 需要删除的专业管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteHncjMajorByIds(Long[] ids)
    {
        hncjMajorMapper.deleteHncjCourseByMIds(ids);
        return hncjMajorMapper.deleteHncjMajorByIds(ids);
    }

    /**
     * 删除专业管理信息
     * 
     * @param id 专业管理主键
     * @return 结果
     */
    @Override
    public int deleteHncjMajorById(Long id)
    {
        hncjMajorMapper.deleteHncjCourseByMId(id);
        return hncjMajorMapper.deleteHncjMajorById(id);
    }

    @Override
    public Long[] getMidBySid(Long[] sids) {
        return hncjMajorMapper.getMidBySid(sids);
    }

    /**
     * 新增课程管理信息
     * 
     * @param hncjMajor 专业管理对象
     */
    public void insertHncjCourse(HncjMajor hncjMajor)
    {
        List<HncjCourse> hncjCourseList = hncjMajor.getHncjCourseList();
        Long id = hncjMajor.getId();
        if (StringUtils.isNotNull(hncjCourseList))
        {
            List<HncjCourse> list = new ArrayList<HncjCourse>();
            for (HncjCourse hncjCourse : hncjCourseList)
            {
                hncjCourse.setMid(id);
                list.add(hncjCourse);
            }
            if (list.size() > 0)
            {
                hncjMajorMapper.batchHncjCourse(list);
            }
        }
    }
}
