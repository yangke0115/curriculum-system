package com.ruoyi.curriculumSystemManagement.service;

import java.util.List;

import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;

/**
 * 课程管理Service接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface IHncjCourseService 
{
    /**
     * 查询课程管理
     * 
     * @param id 课程管理主键
     * @return 课程管理
     */
    public HncjCourse selectHncjCourseById(Long id);

    /**
     * 查询课程管理列表
     * 
     * @param hncjCourse 课程管理
     * @return 课程管理集合
     */
    public List<HncjCourse> selectHncjCourseList(HncjCourse hncjCourse);

    /**
     * 查询课程管理列表
     *
     * @param cname
     * @param mId   专业id
     * @param sId   学校id
     * @return 课程管理集合
     */
    public List<CourseAndMajorAndSchoolVO> listByMajorOrSchool(String cname, Long mId, Long sId);

    /**
     * 新增课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    public int insertHncjCourse(HncjCourse hncjCourse);

    /**
     * 修改课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    public int updateHncjCourse(HncjCourse hncjCourse);

    /**
     * 批量删除课程管理
     * 
     * @param ids 需要删除的课程管理主键集合
     * @return 结果
     */
    public int deleteHncjCourseByIds(Long[] ids);

    /**
     * 删除课程管理信息
     * 
     * @param id 课程管理主键
     * @return 结果
     */
    public int deleteHncjCourseById(Long id);
}
