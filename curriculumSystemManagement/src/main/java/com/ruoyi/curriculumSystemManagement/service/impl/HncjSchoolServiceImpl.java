package com.ruoyi.curriculumSystemManagement.service.impl;

import java.util.HashMap;
import java.util.List;

import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.mapper.HncjSchoolMapper;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.service.IHncjSchoolService;

/**
 * 学校管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
@Service
public class HncjSchoolServiceImpl implements IHncjSchoolService 
{
    @Autowired
    private HncjSchoolMapper hncjSchoolMapper;

    /**
     * 查询学校管理
     * 
     * @param id 学校管理主键
     * @return 学校管理
     */
    @Override
    public HncjSchool selectHncjSchoolById(Long id)
    {
        return hncjSchoolMapper.selectHncjSchoolById(id);
    }

    @Override
    public List<CourseAndMajorAndSchoolVO> selectCourseList(HashMap map) {
        return hncjSchoolMapper.selectCourseList(map);
    }

    /**
     * 查询学校管理列表
     * 
     * @param hncjSchool 学校管理
     * @return 学校管理
     */
    @Override
    public List<HncjSchool> selectHncjSchoolList(HncjSchool hncjSchool)
    {
        return hncjSchoolMapper.selectHncjSchoolList(hncjSchool);
    }

    /**
     * 新增学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertHncjSchool(HncjSchool hncjSchool)
    {
        int rows = hncjSchoolMapper.insertHncjSchool(hncjSchool);
        insertHncjMajor(hncjSchool);
        return rows;
    }

    /**
     * 修改学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateHncjSchool(HncjSchool hncjSchool)
    {
        hncjSchoolMapper.deleteHncjMajorBySId(hncjSchool.getId());
        insertHncjMajor(hncjSchool);
        return hncjSchoolMapper.updateHncjSchool(hncjSchool);
    }

    /**
     * 批量删除学校管理
     * 
     * @param ids 需要删除的学校管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteHncjSchoolByIds(Long[] ids)
    {
        hncjSchoolMapper.deleteHncjMajorBySIds(ids);
        return hncjSchoolMapper.deleteHncjSchoolByIds(ids);
    }

    /**
     * 删除学校管理信息
     * 
     * @param id 学校管理主键
     * @return 结果
     */
    @Override
    public int deleteHncjSchoolById(Long id)
    {
        hncjSchoolMapper.deleteHncjMajorBySId(id);
        return hncjSchoolMapper.deleteHncjSchoolById(id);
    }

    /**
     * 新增专业管理信息
     * 
     * @param hncjSchool 学校管理对象
     */
    public void insertHncjMajor(HncjSchool hncjSchool)
    {
        List<HncjMajor> hncjMajorList = hncjSchool.getHncjMajorList();
        Long id = hncjSchool.getId();
        if (StringUtils.isNotNull(hncjMajorList))
        {
            List<HncjMajor> list = new ArrayList<HncjMajor>();
            for (HncjMajor hncjMajor : hncjMajorList)
            {
                hncjMajor.setSid(id);
                list.add(hncjMajor);
            }
            if (list.size() > 0)
            {
                hncjSchoolMapper.batchHncjMajor(list);
            }
        }
    }
}
