package com.ruoyi.curriculumSystemManagement.service;

import java.util.HashMap;
import java.util.List;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;

/**
 * 学校管理Service接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface IHncjSchoolService 
{
    /**
     * 查询学校管理
     * 
     * @param id 学校管理主键
     * @return 学校管理
     */
    public HncjSchool selectHncjSchoolById(Long id);

    /**
     * 查询学校的专业及课程
     * @param map
     * @return
     */
    List<CourseAndMajorAndSchoolVO> selectCourseList(HashMap map);

    /**
     * 查询学校管理列表
     * 
     * @param hncjSchool 学校管理
     * @return 学校管理集合
     */
    public List<HncjSchool> selectHncjSchoolList(HncjSchool hncjSchool);

    /**
     * 新增学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    public int insertHncjSchool(HncjSchool hncjSchool);

    /**
     * 修改学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    public int updateHncjSchool(HncjSchool hncjSchool);

    /**
     * 批量删除学校管理
     * 
     * @param ids 需要删除的学校管理主键集合
     * @return 结果
     */
    public int deleteHncjSchoolByIds(Long[] ids);

    /**
     * 删除学校管理信息
     * 
     * @param id 学校管理主键
     * @return 结果
     */
    public int deleteHncjSchoolById(Long id);
}
