package com.ruoyi.curriculumSystemManagement.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseSetVO;
import com.ruoyi.curriculumSystemManagement.mapper.HncjTextbookMapper;
import com.ruoyi.curriculumSystemManagement.service.ICourseSetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: songbiao
 */
@Service
public class CourseSetServiceImpl implements ICourseSetService {

    @Resource
    private RedisCache redisCache;

    private String prefixStr = "major:%s_%s";

    @Resource
    private HncjTextbookMapper textbookMapper;

    @Override
    public AjaxResult generateSC(CourseSetVO courseSetVO) {
        Long[] mids = courseSetVO.getMid();
        Integer granularity = courseSetVO.getGranularity();
        //取出缓存交集
        String mid_key = "join:"+Arrays.toString(mids)+"_"+granularity;
        List<HncjTextbook> cacheJoin = redisCache.getCacheList(mid_key);
        if (cacheJoin != null && !cacheJoin.isEmpty())
            return AjaxResult.success(cacheJoin);
        List<List<HncjTextbook>> allTextBooks = new ArrayList<>();
        //所有专业课程
        for (Long  mid: mids) {
            //先从缓存冲读取
            String cache_key = String.format(prefixStr, mid, granularity);
            List<HncjTextbook> list = redisCache.getCacheList(cache_key);
            if (list!=null && !list.isEmpty()) {
                allTextBooks.add(list);
            } else {
                List<HncjTextbook> textbooks = textbookMapper.getHncjTextbooksByMid(mid.intValue(),granularity);
                allTextBooks.add(textbooks);
                //存15分钟
                redisCache.setCacheList(cache_key,textbooks);
                redisCache.expire(cache_key, 15,TimeUnit.MINUTES);
            }
        }
        if (allTextBooks.isEmpty())
            return AjaxResult.error("无数据");
        //取交集
        List<HncjTextbook> joinData = allTextBooks.get(0);
        for (int i = 0; i < allTextBooks.size()-1; i++) {//首先遍历所有专业
            ArrayList<HncjTextbook> temp = new ArrayList<>();
            List<HncjTextbook> target = allTextBooks.get(i + 1);
            for (HncjTextbook hncjTextbook : joinData) {//遍历专业所有教程/后是交集
                List<HncjTextbook> collect = target.parallelStream().
                        filter(item -> {
                            if (item.getName().equals(hncjTextbook.getName())) {
                                temp.add(hncjTextbook);
                                return true;
                            }
                            return false;
                        }).
                        collect(Collectors.toList());
                temp.addAll(collect);
            }
            //循环取出课程交集
            joinData = temp.stream().distinct().collect(Collectors.toList());
        }
        if (joinData.isEmpty())
            return AjaxResult.success("没有交集");
        List<String> parents = new ArrayList<>();
        List<HncjTextbook> temp = joinData;
        //父节点
        for (Integer i = 1; i < granularity; i++) {
            for (HncjTextbook joinDatum : temp)
                parents.add(joinDatum.getParent());
            temp = textbookMapper.listByCodes(parents.stream().distinct().collect(Collectors.toList()));
            joinData.addAll(temp);
            parents.clear();
        }
        //升序
        Collections.sort(joinData, new Comparator<HncjTextbook>() {
            @Override
            public int compare(HncjTextbook b1, HncjTextbook b2) {
                return b1.getCode().compareTo(b2.getCode());
            }
        });
        //缓存交集数据
        redisCache.setCacheList(mid_key, joinData);
        redisCache.expire(mid_key, 15,TimeUnit.MINUTES);
        return AjaxResult.success(joinData);
    }

    @Override
    public AjaxResult generateSM(CourseSetVO courseSetVO) {
        Long[] mid = courseSetVO.getMid();
        if (mid == null || mid.length == 0) {
            return AjaxResult.success("无数据");
        }
        return AjaxResult.success(textbookMapper.getSM(courseSetVO));
    }
}
