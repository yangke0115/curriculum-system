package com.ruoyi.curriculumSystemManagement.service;

import java.util.List;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.vo.MajorAndSchoolVO;

/**
 * 专业管理Service接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface IHncjMajorService 
{
    /**
     * 查询专业管理
     * 
     * @param id 专业管理主键
     * @return 专业管理
     */
    public HncjMajor selectHncjMajorById(Long id);

    /**
     * 查询专业管理列表
     * 
     * @param hncjMajor 专业管理
     * @return 专业管理集合
     */
    public List<MajorAndSchoolVO> selectMajorVOList(HncjMajor hncjMajor);

    /**
     * 查询专业管理列表
     *
     * @param hncjMajor 专业管理
     * @return 专业管理集合
     */
    public List<HncjMajor> selectMajorList(HncjMajor hncjMajor);

    /**
     * 新增专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    public int insertHncjMajor(HncjMajor hncjMajor);

    /**
     * 修改专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    public int updateHncjMajor(HncjMajor hncjMajor);

    /**
     * 批量删除专业管理
     * 
     * @param ids 需要删除的专业管理主键集合
     * @return 结果
     */
    public int deleteHncjMajorByIds(Long[] ids);

    /**
     * 删除专业管理信息
     * 
     * @param id 专业管理主键
     * @return 结果
     */
    public int deleteHncjMajorById(Long id);

    Long[] getMidBySid(Long[] sid);
}
