package com.ruoyi.curriculumSystemManagement.service;

import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseBookAndBookVO;

import java.util.List;

/**
 * (HncjCourseBook)表服务接口
 *
 * @author makejava
 * @since 2021-11-10 11:22:23
 */
public interface IHncjCourseBookService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HncjCourseBook queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<HncjCourseBook> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param hncjCourseBook 实例对象
     * @return 实例对象
     */
    HncjCourseBook insert(HncjCourseBook hncjCourseBook);

    /**
     * 修改数据
     *
     * @param hncjCourseBook 实例对象
     * @return 实例对象
     */
    HncjCourseBook update(HncjCourseBook hncjCourseBook);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<CourseBookAndBookVO> listByCourseId(HncjCourseBook courseBook);

    boolean delete(HncjCourseBook courseBook);

}