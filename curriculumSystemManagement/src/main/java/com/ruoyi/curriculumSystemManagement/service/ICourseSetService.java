package com.ruoyi.curriculumSystemManagement.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseSetVO;

public interface ICourseSetService {
    AjaxResult generateSC(CourseSetVO generateScVO);
    AjaxResult generateSM(CourseSetVO courseSetVO);
}
