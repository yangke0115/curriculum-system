package com.ruoyi.curriculumSystemManagement.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.mapper.HncjTextbookMapper;
import com.ruoyi.curriculumSystemManagement.service.IHncjTextbookService;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 *
 */
@Service
public class HncjTextbookServiceImpl implements IHncjTextbookService {

    @Resource
    private HncjTextbookMapper textbookMapper;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<HncjTextbook> listBook(HncjTextbook textbook) {
        if (Strings.isBlank(textbook.getParent())) {
            //只查书籍
            textbook.setParent("0");
        }
        return textbookMapper.queryAll(textbook);
    }

    @Override
    public List<HncjTextbook> listDetail(String isbn) {
        HncjTextbook queryWrapper = new HncjTextbook();
        queryWrapper.setIsbn(isbn);
        List<HncjTextbook> textbooks = textbookMapper.queryAll(queryWrapper);
        return textbooks;
    }

    @Override
    public boolean insertBookNode(HncjTextbook textbook) {
        return textbookMapper.insert(textbook)>0;
    }

    @Override
    public boolean updateBookNode(HncjTextbook textbook) {
        return textbookMapper.update(textbook)>0;
    }

    /**
     * 需要考虑到级联删除：比如删除章需要将所有小节全部删除
     * @param textbook
     * @return
     */
    @Transactional
    @Override
    public boolean removeBook(HncjTextbook textbook) {
        String code = textbook.getCode();
        textbook.setVia(code);
        Assert.isTrue(textbookMapper.deleteById(code)>0,"没有找到要删除的数据！");
        //删除子节点
        log.info("正在删除code:[{}]的子节点=========>影响行数:{}",code,textbookMapper.deleteByVia(textbook));
        return true;
    }

    @Override
    public List<HncjTextbook> listByCodes(String codes) {
//        Assert.isTrue(Strings.isNotBlank(codes),"code不能为空");
        if (Strings.isBlank(codes))
            return null;
        return textbookMapper.listByCodes(Arrays.asList(codes.split(",")));
    }

    @Override
    public boolean insertList(List<HncjTextbook> textbooks) {
        return textbookMapper.insertList(textbooks)>0;
    }

    public HncjTextbook getParent(HncjTextbook hncjTextbook) {
        return textbookMapper.getParent(hncjTextbook.getParent());
    }

}