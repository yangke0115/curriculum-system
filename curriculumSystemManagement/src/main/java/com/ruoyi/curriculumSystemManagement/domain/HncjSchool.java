package com.ruoyi.curriculumSystemManagement.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学校管理对象 hncj_school
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public class HncjSchool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String sname;

    /** 专业管理信息 */
    private List<HncjMajor> hncjMajorList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSname(String sname) 
    {
        this.sname = sname;
    }

    public String getSname() 
    {
        return sname;
    }

    public List<HncjMajor> getHncjMajorList()
    {
        return hncjMajorList;
    }

    public void setHncjMajorList(List<HncjMajor> hncjMajorList)
    {
        this.hncjMajorList = hncjMajorList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sname", getSname())
            .append("hncjMajorList", getHncjMajorList())
            .toString();
    }
}
