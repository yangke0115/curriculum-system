package com.ruoyi.curriculumSystemManagement.domain;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * 
 * @TableName hncj_textbook
 */
public class HncjTextbook implements Serializable {
    /**
     * 课程编号
     */
    private String code;

    /**
     * 学科编码
     */
    @NotNull(message = "isbn不能为空")
    private String isbn;

    /**
     * 名称
     */
    @NotNull(message = "学科名称不能为空")
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 父结点
     */
    private String parent;

    /**
     * 途径
     */
    private String via;

    /**
     * 层次
     */
    private String type;


    private static final long serialVersionUID = 1L;

    /**
     * 课程编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 课程编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 学科编码
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * 学科编码
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 父结点
     */
    public String getParent() {
        return parent;
    }

    /**
     * 父结点
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * 途径
     */
    public String getVia() {
        return via;
    }

    /**
     * 途径
     */
    public void setVia(String via) {
        this.via = via;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HncjTextbook{" +
                "code='" + code + '\'' +
                ", isbn='" + isbn + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", parent='" + parent + '\'' +
                ", via='" + via + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HncjTextbook that = (HncjTextbook) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(isbn, that.isbn) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(parent, that.parent) &&
                Objects.equals(via, that.via) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, isbn, name, description, parent, via, type);
    }
}