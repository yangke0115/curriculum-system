package com.ruoyi.curriculumSystemManagement.domain;

import java.io.Serializable;

/**
 * (HncjCourseBook)实体类
 *
 * @author makejava
 * @since 2021-11-10 11:22:17
 */
public class HncjCourseBook implements Serializable {
    private static final long serialVersionUID = 576916741282189725L;
    /**
    * id
    */
    private Integer id;
    /**
    * 课程id
    */
    private Integer cId;
    /**
    * 教材id
    */
    private String textId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCId() {
        return cId;
    }

    public void setCId(Integer cId) {
        this.cId = cId;
    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }

}