package com.ruoyi.curriculumSystemManagement.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 专业管理对象 hncj_major
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public class HncjMajor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String mname;

    /** 学校id */
    @Excel(name = "学校id")
    private Long sid;

    /** 课程管理信息 */
    private List<HncjCourse> hncjCourseList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMname(String mname) 
    {
        this.mname = mname;
    }

    public String getMname() 
    {
        return mname;
    }
    public void setSid(Long sid) 
    {
        this.sid = sid;
    }

    public Long getSid() 
    {
        return sid;
    }

    public List<HncjCourse> getHncjCourseList()
    {
        return hncjCourseList;
    }

    public void setHncjCourseList(List<HncjCourse> hncjCourseList)
    {
        this.hncjCourseList = hncjCourseList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mname", getMname())
            .append("sid", getSid())
            .append("hncjCourseList", getHncjCourseList())
            .toString();
    }
}
