package com.ruoyi.curriculumSystemManagement.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 课程管理对象 hncj_course
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public class HncjCourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 课程名字 */
    @Excel(name = "课程名字")
    private String cname;

    /** 专业id */
    @Excel(name = "专业id")
    private Long mid;

    private String codes;

    /** $table.subTable.functionName信息 */
    private List<HncjTextbook> hncjTextbookList;

    @Override
    public String toString() {
        return "HncjCourse{" +
                "id=" + id +
                ", cname='" + cname + '\'' +
                ", mid=" + mid +
                ", codes='" + codes + '\'' +
                ", hncjTextbookList=" + hncjTextbookList +
                '}';
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public List<HncjTextbook> getHncjTextbookList() {
        return hncjTextbookList;
    }

    public void setHncjTextbookList(List<HncjTextbook> hncjTextbookList) {
        this.hncjTextbookList = hncjTextbookList;
    }
}
