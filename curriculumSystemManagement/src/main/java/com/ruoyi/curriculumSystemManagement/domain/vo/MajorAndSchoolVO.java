package com.ruoyi.curriculumSystemManagement.domain.vo;

import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;

/**
 * @Description:
 * @Author: songbiao
 */
public class MajorAndSchoolVO extends HncjMajor {

    private HncjSchool school;

    public HncjSchool getSchool() {
        return school;
    }

    public void setSchool(HncjSchool school) {
        this.school = school;
    }
}
