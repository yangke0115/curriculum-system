package com.ruoyi.curriculumSystemManagement.domain.vo;

import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;

/**
 * @Description:
 * @Author: songbiao
 */
public class CourseBookAndBookVO extends HncjCourseBook {

    private HncjTextbook textbook;

    public HncjTextbook getTextbook() {
        return textbook;
    }

    public void setTextbook(HncjTextbook textbook) {
        this.textbook = textbook;
    }
}
