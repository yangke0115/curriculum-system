package com.ruoyi.curriculumSystemManagement.domain.vo;

import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;

/**
 * @Description:
 * @Author: songbiao
 */
public class CourseAndMajorAndSchoolVO extends HncjCourse {

    private HncjSchool school;

    private HncjMajor major;

    public HncjSchool getSchool() {
        return school;
    }

    public void setSchool(HncjSchool school) {
        this.school = school;
    }

    public HncjMajor getMajor() {
        return major;
    }

    public void setMajor(HncjMajor major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "CourseAndMajorAndSchoolVO{" +
                "school=" + school +
                ", major=" + major +
                '}';
    }
}
