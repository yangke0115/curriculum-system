package com.ruoyi.curriculumSystemManagement.domain.vo;

public class CourseSetVO {
    Long[] sid;
    Long[] mid;
    Integer granularity;

    public Long[] getSid() {
        return sid;
    }

    public void setSid(Long[] sid) {
        this.sid = sid;
    }

    public Long[] getMid() {
        return mid;
    }

    public void setMid(Long[] mid) {
        this.mid = mid;
    }

    public Integer getGranularity() {
        return granularity;
    }

    public void setGranularity(Integer granularity) {
        this.granularity = granularity;
    }
}
