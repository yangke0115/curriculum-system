package com.ruoyi.curriculumSystemManagement.mapper;

import java.util.List;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;

/**
 * 专业管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface HncjMajorMapper 
{
    /**
     * 查询专业管理
     * 
     * @param id 专业管理主键
     * @return 专业管理
     */
    public HncjMajor selectHncjMajorById(Long id);

    /**
     * 查询专业管理列表
     * 
     * @param hncjMajor 专业管理
     * @return 专业管理集合
     */
    public List<HncjMajor> selectHncjMajorList(HncjMajor hncjMajor);

    /**
     * 新增专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    public int insertHncjMajor(HncjMajor hncjMajor);

    /**
     * 修改专业管理
     * 
     * @param hncjMajor 专业管理
     * @return 结果
     */
    public int updateHncjMajor(HncjMajor hncjMajor);

    /**
     * 删除专业管理
     * 
     * @param id 专业管理主键
     * @return 结果
     */
    public int deleteHncjMajorById(Long id);

    /**
     * 批量删除专业管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjMajorByIds(Long[] ids);

    /**
     * 批量删除课程管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjCourseByMIds(Long[] ids);
    
    /**
     * 批量新增课程管理
     * 
     * @param hncjCourseList 课程管理列表
     * @return 结果
     */
    public int batchHncjCourse(List<HncjCourse> hncjCourseList);
    

    /**
     * 通过专业管理主键删除课程管理信息
     * 
     * @param id 专业管理ID
     * @return 结果
     */
    public int deleteHncjCourseByMId(Long id);

    Long[] getMidBySid(Long[] array);
}
