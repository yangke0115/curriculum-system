package com.ruoyi.curriculumSystemManagement.mapper;

import java.util.List;
import com.ruoyi.curriculumSystemManagement.domain.HncjCourse;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;

/**
 * 课程管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface HncjCourseMapper 
{
    /**
     * 查询课程管理
     * 
     * @param id 课程管理主键
     * @return 课程管理
     */
    public HncjCourse selectHncjCourseById(Long id);

    /**
     * 查询课程管理列表
     * 
     * @param hncjCourse 课程管理
     * @return 课程管理集合
     */
    public List<HncjCourse> selectHncjCourseList(HncjCourse hncjCourse);

    /**
     * 新增课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    public int insertHncjCourse(HncjCourse hncjCourse);

    /**
     * 修改课程管理
     * 
     * @param hncjCourse 课程管理
     * @return 结果
     */
    public int updateHncjCourse(HncjCourse hncjCourse);

    /**
     * 删除课程管理
     * 
     * @param id 课程管理主键
     * @return 结果
     */
    public int deleteHncjCourseById(Long id);

    /**
     * 批量删除课程管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjCourseByIds(Long[] ids);

    /**
     * 批量删除${subTable.functionName}
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjTextbookByCIds(Long[] ids);
    
    /**
     * 批量新增${subTable.functionName}
     * 
     * @param hncjTextbookList ${subTable.functionName}列表
     * @return 结果
     */
    public int batchHncjTextbook(List<HncjTextbook> hncjTextbookList);
    

    /**
     * 通过课程管理主键删除${subTable.functionName}信息
     * 
     * @param id 课程管理ID
     * @return 结果
     */
    public int deleteHncjTextbookByCId(Long id);
}
