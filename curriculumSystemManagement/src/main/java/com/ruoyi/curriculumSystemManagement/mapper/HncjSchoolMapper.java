package com.ruoyi.curriculumSystemManagement.mapper;

import java.util.HashMap;
import java.util.List;
import com.ruoyi.curriculumSystemManagement.domain.HncjSchool;
import com.ruoyi.curriculumSystemManagement.domain.HncjMajor;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseAndMajorAndSchoolVO;

/**
 * 学校管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-06
 */
public interface HncjSchoolMapper 
{
    /**
     * 查询学校管理
     * 
     * @param id 学校管理主键
     * @return 学校管理
     */
    public HncjSchool selectHncjSchoolById(Long id);

    List<CourseAndMajorAndSchoolVO> selectCourseList(HashMap map);

    /**
     * 查询学校管理列表
     * 
     * @param hncjSchool 学校管理
     * @return 学校管理集合
     */
    public List<HncjSchool> selectHncjSchoolList(HncjSchool hncjSchool);

    /**
     * 新增学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    public int insertHncjSchool(HncjSchool hncjSchool);

    /**
     * 修改学校管理
     * 
     * @param hncjSchool 学校管理
     * @return 结果
     */
    public int updateHncjSchool(HncjSchool hncjSchool);

    /**
     * 删除学校管理
     * 
     * @param id 学校管理主键
     * @return 结果
     */
    public int deleteHncjSchoolById(Long id);

    /**
     * 批量删除学校管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjSchoolByIds(Long[] ids);

    /**
     * 批量删除专业管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHncjMajorBySIds(Long[] ids);
    
    /**
     * 批量新增专业管理
     * 
     * @param hncjMajorList 专业管理列表
     * @return 结果
     */
    public int batchHncjMajor(List<HncjMajor> hncjMajorList);
    

    /**
     * 通过学校管理主键删除专业管理信息
     * 
     * @param id 学校管理ID
     * @return 结果
     */
    public int deleteHncjMajorBySId(Long id);
}
