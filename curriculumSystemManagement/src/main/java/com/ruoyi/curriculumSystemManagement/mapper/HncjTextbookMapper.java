package com.ruoyi.curriculumSystemManagement.mapper;


import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.domain.vo.CourseSetVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.ruoyi.curriculumSystemManagement.domain.HncjTextbook
 */
public interface HncjTextbookMapper{


    HncjTextbook getParent(String parent);

    List<HncjTextbook> queryAll(HncjTextbook hncjTextbook);


    int deleteById(String code);

    int deleteByVia(HncjTextbook textbook);

    int update(HncjTextbook hncjTextbook);

    int insert(HncjTextbook textbook);

    int insertList(List<HncjTextbook> textbooks);

    HncjTextbook queryById(String code);

    List<HncjTextbook> listByCodes(List<String> codes);

    List<HncjTextbook> getHncjTextbooksByISBN(String isbn);

    List<HncjTextbook> getHncjTextbooks(CourseSetVO courseSetVO);

    List<HncjTextbook> getHncjTextbooksByMid(@Param("mid") Integer mid,@Param("granularity") Integer granularity);

    /*
    * 求专业教材并集
    * */
    List<HncjTextbook> getSM(CourseSetVO courseSetVO);

}




