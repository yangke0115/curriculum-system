package com.ruoyi.curriculumSystemManagement.mapper;

import com.ruoyi.curriculumSystemManagement.domain.HncjCourseBook;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (HncjCourseBook)表数据库访问层
 *
 * @author makejava
 * @since 2021-11-10 11:22:21
 */
public interface HncjCourseBookMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HncjCourseBook queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<HncjCourseBook> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param hncjCourseBook 实例对象
     * @return 对象列表
     */
    List<HncjCourseBook> queryAll(HncjCourseBook hncjCourseBook);

    /**
     * 新增数据
     *
     * @param hncjCourseBook 实例对象
     * @return 影响行数
     */
    int insert(HncjCourseBook hncjCourseBook);

    /**
     * 修改数据
     *
     * @param hncjCourseBook 实例对象
     * @return 影响行数
     */
    int update(HncjCourseBook hncjCourseBook);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    int delete(HncjCourseBook courseBook);

}