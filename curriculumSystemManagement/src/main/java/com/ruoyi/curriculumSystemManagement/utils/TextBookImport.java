package com.ruoyi.curriculumSystemManagement.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.service.IHncjTextbookService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description:
 * @Author: songbiao
 */
@Component
public class TextBookImport {

    private String isbn;
    private String courseCode;
    private int deep;//递归深度

    private List<HncjTextbook> textbooks;

    @Resource
    private IHncjTextbookService textbookService;

    private void addRow(String code, String name, String detail, String parent, String via){
        HncjTextbook hncjTextbook = new HncjTextbook();
        hncjTextbook.setCode(code);
        hncjTextbook.setVia(via);
        hncjTextbook.setName(name);
        hncjTextbook.setCode(code);
        hncjTextbook.setDescription(detail);
        hncjTextbook.setParent(parent);
        hncjTextbook.setIsbn(isbn);
        textbooks.add(hncjTextbook);
    }


    public boolean process(JSONObject jsonStruct) {
        deep = -1;
        textbooks = new ArrayList<>();
        //取出书名
        String bookName = jsonStruct.getString("书名");
        //取出ISBN
        isbn = jsonStruct.getString("ISBN");
        //取出类别
        String detail = jsonStruct.getString("类别");
        //章，节，知识点
        String[] suffixCode = new String[] {"00","00","00"};
        //课程编号
        courseCode = isbn +"000000";
        //目录
        JSONObject contentSet = jsonStruct.getJSONObject("目录");

        //目录处理
        content_process(contentSet.getInnerMap(), courseCode,"0,"+courseCode,suffixCode);

        //课程处理
        addRow(courseCode,bookName,detail,"0","0");

        return textbookService.insertList(textbooks);
    }

    private void content_process(Map itemSet, String parentCode, String via, String[] parentSuffixCode) {
        deep++;
        Set<String> keySet = itemSet.keySet();
        for (String key : keySet) {
            String[] curSuffixCode = getCurSuffixCode(parentSuffixCode,deep);
            String curCode = isbn +curSuffixCode[0]+curSuffixCode[1]+curSuffixCode[2];
            Object object = itemSet.get(key);
            //如果有子节点的话 就进行递归
            if(object instanceof JSONObject) {
                content_process(((JSONObject)object).getInnerMap(), curCode,via+","+curCode,curSuffixCode);
                //重置章节点编号
                parentSuffixCode[deep]="00";
                deep--;
            }else if(object instanceof JSONArray) {//叶子节点直接入库处理
                deep++;
                JSONArray itemArray = (JSONArray) object;
                for (int i = 0; i < itemArray.size(); i++) {
                    String item = String.valueOf(itemArray.get(i));
                    String[] curItemSuffixCode = getCurSuffixCode(parentSuffixCode,deep);
                    String curItemCode = curItemSuffixCode[0]+curItemSuffixCode[1]+curItemSuffixCode[2];
                    addRow(isbn +curItemCode , item, "", curCode, via+","+curCode);
                }
                //重置叶子节点编号
                parentSuffixCode[deep]="00";
                deep--;
            }
            //当前趟递归结束后
            addRow(curCode, key, "", parentCode, via);
        }
    }


    private static String[] getCurSuffixCode(String[] suffixCode,int deep) {
        int sc = Integer.parseInt(suffixCode[deep]);
        suffixCode[deep] = ++sc<10?"0"+sc:sc+"";
        return suffixCode;
    }


}
