import com.ruoyi.RuoYiApplication;
import com.ruoyi.curriculumSystemManagement.domain.HncjTextbook;
import com.ruoyi.curriculumSystemManagement.service.ICourseSetService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: songbiao
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = RuoYiApplication.class)
public class CourseSetServiceImplTest {

    @Resource
    private ICourseSetService courseSetService;

    @Test
    public void test(){
//        generateSc.generateSC(null, new Long[]{11L,12L}, 3);
    }

    @Test
    public void test2(){
        List<List<HncjTextbook>> allTextBooks = new ArrayList<>();
        List<HncjTextbook> list1 = new ArrayList<>();
        List<HncjTextbook> list2 = new ArrayList<>();

        HncjTextbook textbook1 = new HncjTextbook();
        textbook1.setIsbn("1111111111111111");
        textbook1.setName("123123");
        HncjTextbook textbook2 = new HncjTextbook();
        textbook2.setIsbn("1111111111111111");
        textbook2.setName("123123");
        list1.add(textbook1);
//        list1.add(textbook2);
//        list2.add(textbook1);
        list2.add(textbook2);

        allTextBooks.add(list1);
        allTextBooks.add(list2);

        Optional<List<HncjTextbook>> result = allTextBooks.parallelStream()
                .filter(elementList -> !elementList.isEmpty())
                .reduce((a, b) -> {
                    a.retainAll(b);
                    return a;
                });

        List<HncjTextbook> hncjTextbooks = result.orElse(new ArrayList<>());
        for (HncjTextbook hncjTextbook : hncjTextbooks) {
            System.out.println(hncjTextbook);
        }
    }

    @Test
    public  void test3(){
        ArrayList<HncjTextbook> objects = new ArrayList<>();
        HncjTextbook b1 = new HncjTextbook();
        b1.setIsbn("123123");
        HncjTextbook b2 = new HncjTextbook();
        b2.setIsbn("123123");
        objects.add(b1);
        objects.add(b2);

        System.out.println(objects.stream().distinct().collect(Collectors.toList()));
    }

}
